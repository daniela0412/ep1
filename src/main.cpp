#include <iostream>
#include <fstream>
#include "carrinho.hpp"
#include "cliente.hpp"
#include <vector>
using namespace std;
void menu();
bool file_exists_menu(const string &name, string cpf);
bool existe_cliente();

int main (){
    
        Pessoa pessoa1;
 bool login_pessoa = pessoa1.cadastro();

    if(login_pessoa){
        int opcao;
        system("clear");
        menu();
        cout << endl;
        cin >> opcao;

        do{
            switch(opcao){

                case 1:{
                    Cliente cliente1;
                    bool cadastro_cliente = cliente1.cadastro();
                    if(cadastro_cliente){
                        //system("clear");
                        menu();
                        cout << endl;
                        cin >> opcao;
                    }
                    break;
                }

                case 2:{
                    Compra compra1;
                    bool cadastro_compra = compra1.cadastro();
                    if(cadastro_compra){
                        menu();
                        cout << endl;
                        cin >> opcao;
                    }
                    break;
                }

                case 3:{
                    Carrinho vendas;
                    bool venda_realizada = vendas.venda();
                    if(venda_realizada){
                        system("clear");
                        menu();
                        cin >> opcao;
                    }
                    break;
                }

                case 4:{
                    if(existe_cliente()){
                        // Modo recomendação;
                        system("clear");
                        menu();
                        cout << endl;
                        cin >> opcao;

                    }
                    break;
                }

                
            }
        }while(opcao != 5);
    
    }
    return 0;
}



void menu(){
    

    cout << "# # # # # # # # # # # # # # # # #"<< endl;
    cout << "#1 - Cadastrar cliente          #" << endl;
    cout << "#2 - Cadastrar compra           #" << endl;
    cout << "#3 - Modo venda                 #" << endl;
    cout << "#4 - Modo recomendação          #" << endl;
    cout << "#5 - Sair                       #" << endl;
    cout << "# # # # # # # # # # # # # # # # #" << endl;
}

bool file_exists_menu(const string &name, string cpf){
    cpf += ".txt";
    vector<string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);

    for(string NomeArq : v){
        if(NomeArq == cpf) return true;
    }

    return false;
}

bool existe_cliente(){

    string cpf, texto1, texto2;
    int temp;
    ofstream saida;
    ifstream entrada1;
    ifstream entrada2;

    cout << "Para entrar no modo recomendação." << endl << "Digite o CPF:" << endl;
    cin >> cpf;
    if(!file_exists_menu("../ep1/doc/Clientes/", cpf)){
        do{
            cout << "Cliente não cadastrado! Selecione uma opcao:" << endl;
            cout << "1 - Tentar novamente." << endl << "2 - Voltar para o menu." << endl;
            cin >> temp;
            if(temp == 1){
            cout << "Digite o CPF do cliente novamente:" << endl;
            cin >> cpf;
            }else{
                return true;
            }
        }while(!file_exists_menu("../ep1/doc/Clientes/", cpf));
    }else{
        system("clear");
        cout << "MODO RECOMENDAÇÃO" << endl << endl;
        entrada1.open("../ep1/doc/Clientes/" + cpf + ".txt");
        if(entrada1.is_open()){
            if(getline(entrada1, texto1)){
                cout << texto1 << ":" << endl << endl;
            }

            

            temp = 0;
            for(int line = 1; getline(entrada1,texto1); line++){

                if(line == 1){}
                if(line >= 2){
                    entrada2.open("../ep1/doc/Categorias/" + texto1 + ".txt");
                    if(entrada2.is_open()){
                        while(getline(entrada2, texto2)){
                            temp++;
                            if(temp == 10){
                                cout << endl << "Digite ENTER para voltar ao menu." << endl;
                                getchar(); getchar();
                                return true;
                            }
                            cout << texto2 << endl;
                        }
                        entrada2.close();
                    }
                }
            }       
            cout << endl << "Digite ENTER para voltar ao menu." << endl;
            getchar(); getchar();  
            return true;
            entrada1.close();
        }
    }
    return false;
}
