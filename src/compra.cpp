#include "compra.hpp"

Compra::Compra(){
    this->nome_prod = "";
    this->categoria = "";
    this->qt_prod = 0; 
    this->preco = 0.0;
    this->id_produto = "";
}

Compra::~Compra(){
    
}

void Compra::set_nome_prod(string nome_prod){
    this->nome_prod = nome_prod;
}
string Compra::get_nome_prod(){
    return nome_prod;
}

void Compra::set_id_produto(string id_produto){
    this->id_produto = id_produto;
}
string Compra::get_id_produto(){
    return id_produto;
}

void Compra::set_categoria(string categoria){
    this->categoria = categoria;
}
string Compra::get_categoria(){
    return categoria;
}


void Compra::set_preco(float preco){
    this->preco = preco;
}
float Compra::get_preco(){
    return preco;
}

void Compra::set_qt_prod(int qt_prod){
    this->qt_prod = qt_prod;

}
int Compra::get_qt_prod(){
    return qt_prod;
}


bool Compra::cadastro(){
    ofstream saida;
    int qt_categorias;

    system("clear");
    cout << "CADASTRAR COMPRA" << endl << endl;
    cout << "Para cadastrar a compra, informe:" << endl;
    cout << "ID: "; cin >> id_produto;
    cout << "Nome: "; getline(cin >> ws, nome_prod);
    cout << "Preço: "; cin >> preco;
    cout << "Quantidade da Compra: "; cin >> qt_prod;
    
    saida.open("../doc/Compras/" + id_produto + ".txt", ios_base::app);
    if(saida.is_open()){
        saida << nome_prod << endl << preco << endl << qt_prod << endl;
        saida.close();
    }

    cout << "A compra  possui quantas categorias? "; cin >> qt_categorias;
    for(int i=1; i<=qt_categorias; i++){
        cout << "Categoria " << i << ": "; getline(cin >>  ws, categoria);

        saida.open("../ep1/doc/Compras/" + id_produto + ".txt", ios_base::app);
        if(saida.is_open()){
            saida << categoria << endl;
            saida.close();
        }

        saida.open("../ep1/doc/Compras/Categorias/" + categoria + ".txt", ios_base::app);
        if(saida.is_open()){
            saida << id_produto << endl;
            saida.close();
        }
    }

    system("clear");
    cout << "Compra cadastrada com sucesso!" << endl << endl;
    return true;
}

