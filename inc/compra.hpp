#ifndef COMPRA_HPP
#define COMPRA_HPP

#include <bits/stdc++.h>
#include <sys/types.h>
#include <dirent.h>
using namespace std;

class Compra{

  private:

     string nome_prod;
     string categoria;
     string id_produto;
     int qt_prod;
     float preco;
	

  public:

     Compra();~Compra();

        void set_nome_prod(string nome_prod);
	     string get_nome_prod();
        void set_id_produto(string id_produto);
        string get_id_produto();
        void set_categoria(string categoria);
     	  string get_categoria();
        void set_qt_prod(int qt_prod);
	     int get_qt_prod();
        void set_preco(float preco);
        float get_preco();
        bool cadastro();
};
#endif



