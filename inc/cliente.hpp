#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include<string>
#include "pessoa.hpp"

using namespace std;
class Cliente : public Pessoa{

private:
    bool associado;

public:
Cliente();
~Cliente();
void set_associado(bool associado);
bool get_associado();
bool cadastro();



};
#endif