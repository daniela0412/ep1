#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <bits/stdc++.h>
#include <sys/types.h>
#include <dirent.h>

#include "compra.hpp"
#include "cliente.hpp"

using namespace std;

class Carrinho{

   public:
	
     Carrinho();~Carrinho();

     void add_compra();
     void remov_compra();
     void imprime_carrinho();
     bool venda();
     bool true_or_false();


};
#endif
