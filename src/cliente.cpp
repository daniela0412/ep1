#include "cliente.hpp"
#include <iostream>
using namespace std;

bool file_exists_cliente(const string &nome, string cpf){
        cpf += ".txt";
        vector <string> v;
        DIR* dirp = opendir(nome.c_str());
        struct dirent * dp;
        while ((dp = readdir(dirp)) != NULL){
                v.push_back(dp->d_name);
        }
        closedir(dirp);

        for (string NomeArq:v){
           if(NomeArq == cpf)
                return true;
        }
        return false;
}
Cliente::Cliente(){
    set_nome(" ");
    set_cpf(" ");
    this-> associado = 0;
}

Cliente::~Cliente(){}

void Cliente::set_associado(bool associado){
        this -> associado = associado;
}
bool Cliente::get_associado(){
        return associado;
}
bool Cliente::cadastro(){

  ofstream saida;
  string nome;
  string cpf;
  string y_or_n;
  system("clear");
  cout <<"# # # # # # # # # # # # # # # # # #"<< endl;
  cout <<"# Cadastro do Cliente             #"<<endl;
  cout <<"# Digite o CPF do CLiente         #"<<endl;
  cout <<"# # # # # # # # # # # # # # # # # #"<< endl<<endl;
  cin>> cpf;
  set_cpf(cpf);

  if(file_exists_cliente("../ep1/doc/Clientes/", get_cpf())){
          system("clear");
          cout << "Cliente já cadastrado!"<< endl<< endl;
          return true;
          }
  else{
          saida.open("../ep1/doc/Clientes/" + get_cpf()+ ".txt");
          if(saida.is_open()){
                  cout<< "Insira o nome do cliente:"<<endl;
                  getline(cin>>ws, nome);
                  set_nome(nome);
                  cout << "O cliente é associado a loja? [y/n]"<<endl;
                  cin>>y_or_n;
                  if(y_or_n == "y") associado = 1;
                  saida<< nome<<endl<<associado;
                  saida.close();
                  system("clear");
                  cout << "Cadastrado com sucesso!"<< endl;
                  return true;
          }
  }

        return false;
}
