#include "carrinho.hpp"

ifstream entrada;
ofstream saida;
vector<string> compra1 = {};
vector<int> qt_compra = {};
int associado = 0;
string txt, cpf;

bool t_or_f;
bool Carrinho::true_or_false(){
    return t_or_f;
}

bool file_exists_carrinho(const string &name, string cpf){
    cpf += ".txt";
    vector<string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);

    for(string NomeArq : v){
        if(NomeArq == cpf) return true;
    }

    return false;
}

bool text_exists_carrinho(string name, string dado){

    entrada.open("../ep1/doc/Clientes/" + name + ".txt");

    if(entrada.is_open()){
        while(getline(entrada, txt)){
            if(txt == dado){
                entrada.close();
                return true;
            }
        }
    }
    return false;
}

Carrinho::Carrinho(){
    
}

Carrinho::~Carrinho(){

}

void Carrinho::imprime_carrinho(){

    string nome_compra = "", txt;
    float preco_compra = 0.0, valor_total = 0.0, valor_final = 0.0;
    int qtd_compra_estoque = 0, line; 
    unsigned int i;

    for(i = 0; i < compra1.size(); i++){
        entrada.open("../ep1/doc/Compras/" + compra1[i] + ".txt");
        if(entrada.is_open()){
            for(line = 1; getline(entrada, txt) && line <= 3; line++){
                if(line == 3) qtd_compra_estoque = stoi(txt);
            }
            if(qt_compra[i] > qtd_compra_estoque){
                compra1.clear();
                qt_compra.clear();
                system("clear");
                cout << "ERROR! A compra apresenta algum produto em quantidade maior que a existente no estoque. Compra cancelada!" << endl << endl;
                cout << "Execute o programa novamente caso queira realizar uma nova venda." << endl;
                t_or_f = true;
                return;
            } 
        }
        entrada.close();
    }
    qtd_compra_estoque = 0;

    system("clear");
    cout << "Venda realizada com sucesso!" << endl << endl;

    cout << "------------------ CARRINHO --------------------" << endl << endl;

    for(i = 0; i < compra1.size(); i++){
        entrada.open("../ep1/doc/Compras/" + compra1[i] + ".txt");
        if(entrada.is_open()){
            for(line = 1; getline(entrada, txt) && line <= 3; line++){
                if(line == 1) nome_compra = txt;
                if(line == 2) preco_compra = stof(txt);
                if(line == 3) qtd_compra_estoque = stoi(txt);
            }
            cout << compra1[i] << " - " << nome_compra << ": " << qt_compra[i] << " x R$" << preco_compra << endl;
            valor_total = (qt_compra[i]*preco_compra) + valor_total;
            if(qt_compra[i] > 1){
                saida.open("../ep1/doc/Compras/" + compra1[i] + ".txt");
                if(saida.is_open()){
                    saida << nome_compra << endl << preco_compra << endl << qtd_compra_estoque - qt_compra[i];
                    saida.close();
                }
            }else{
                saida.open("../ep1/doc/Compras/" + compra1[i] + ".txt");
                if(saida.is_open()){
                    saida << nome_compra << endl << preco_compra << endl << qtd_compra_estoque - 1;
                    saida.close();
                }
            }
            entrada.close();

        }
    }
    nome_compra = "";
    preco_compra = 0.0;
    qtd_compra_estoque = 0;

    cout << "O valor total é de R$ " << valor_total << endl;
    if(associado == 1){
        valor_final = valor_total - (valor_total * 0.15);
        cout << "O cliente é associado! Por tanto, há 15% de desconto no valor total da compra!" << endl;
        cout << "Por tanto, o valor a ser pago é de R$ " << valor_final << endl << endl;
        cout << "Execute o programa novamente caso queira realizar uma nova venda." << endl;
        valor_final = 0.0;
        valor_total = 0.0;
        t_or_f = true;
        return;
    }else{
        cout << "Como o cliente não é associado, não há desconto na compra." << endl;
        cout << "Por tanto, o valor a ser pago é o mesmo do valor total: R$ " << valor_total << endl << endl;
        cout << "Execute o programa novamente caso queira realizar uma nova venda." << endl;
        valor_total = 0.0;
        t_or_f = true;
        return;
    }
}

void Carrinho::add_compra(){

    string id_compra_add; 
    string nome_compra;
    int qtd_compra_estoque = 0, qtd_compra_desejada;
    float preco_compra = 0;
    int op, cont = 0;

    system("clear");
    cout << "ADICIONANDO UMA NOVA COMPRA AO CARRINHO" << endl << endl;

    if(compra1.empty()){
        cout << "O carrinho está vazio! Adicione uma compra no carrinho." << endl << endl;
        cout << "Digite o ID da compra que deseja adicionar: "; cin >> id_compra_add;
    }else{
        cout << "Digite o ID da compra que deseja adicionar: "; cin >> id_compra_add;
    }

    if(file_exists_carrinho("../ep1/doc/Compras/", id_compra_add)){

        entrada.open("../ep1/doc/Compras/" + id_compra_add + ".txt");
        if(entrada.is_open()){
            for(int line = 1; getline(entrada, txt) && line <= 3; line++){
                if(line == 1) nome_compra = txt;
                if(line == 2) preco_compra = stof(txt);
                if(line == 3) qtd_compra_estoque = stoi(txt);
            }
            system("clear");
            cout << "Compra encontrada com sucesso! Segue abaixo as informações referentes ao produto de ID " << id_compra_add << endl;
            cout << "Nome da compra: " << nome_compra << endl;
            cout << "Preço de cada und: R$ " << preco_compra << endl;
            cout << "Quantidade em estoque: " << qtd_compra_estoque << endl << endl;
            cout << "Quantas und de " << nome_compra << " deseja adicionar no carrinho?" << endl;
            cin >> qtd_compra_desejada;
            
            if(!compra1.empty()){
                for(unsigned int i=0; i<compra1.size(); i++){
                    if(compra1[i] == id_compra_add){
                        cont = 1;
                        qt_compra[i] += qtd_compra_desejada;
                        break;
                    }
                }
                if(cont == 0){
                    compra1.push_back(id_compra_add);
                    qt_compra.push_back(qtd_compra_desejada);
                }else{
                    cont = 0;
                }
            }else{
                compra1.push_back(id_compra_add);
                qt_compra.push_back(qtd_compra_desejada);
            }

            if(qtd_compra_desejada > 1){
                cout << endl;
                cout << "Foram adicionados " << qtd_compra_desejada << " und de " << nome_compra << " ao carrinho!" << endl << endl;
            }else{
                cout << endl;
                cout << "Foi adicionado 1 und de " << nome_compra << " ao carrinho!" << endl << endl;
            }
            cout <<"# # # # # # # # # # # # # # # # # # # # # # #"<< endl;
            cout <<"#Escolha uma opção:                         #" << endl;
            cout <<"#1 - Adicionar mais uma compra no carrinho. #" << endl;
            cout <<"#2 - Remover uma compra do carrinho.        #" << endl;
            cout <<"#3 - Finalizar venda.                       #" << endl;
            cout <<"# # # # # # # # # # # # # # # # # # # # # # #"<< endl<<endl;
            cin >> op;

            if(op == 1){
                add_compra();
            }
            else if(op == 2){
                remov_compra();
            }
            else{
                imprime_carrinho();
            }
            entrada.close();
        }
    }else{
        cout << "Produto não existe no estoque!" << endl;
        cout << "Digite ENTER para adicionar outra compra ao carrinho." << endl;
        getchar(); getchar();
        add_compra();
    }
}

void Carrinho::remov_compra(){

    string id_compra_remove, nome_compra;
    int op;

    system("clear");
    cout << "REMOVENDO COMPRAS DO CARRINHO" << endl << endl;
    if(compra1.empty()){
        cout << "O carrinho já está vazio! Digite ENTER para ir ao modo de adição de produtos ao carrinho." << endl;
        getchar(); getchar();
        add_compra();
    }else{
        cout << "Digite o ID da compra que deseja remover do carrinho: "; cin >> id_compra_remove;
        for(unsigned int i = 0; i < compra1.size(); i++){
            if(compra1[i] == id_compra_remove){
                compra1.erase(compra1.begin()+i);
                qt_compra.erase(qt_compra.begin()+i);
                entrada.open("../ep1/doc/Compras/" + id_compra_remove + ".txt");
                if(entrada.is_open()){
                    if(getline(entrada, txt)){
                        nome_compra = txt;
                    }
                    cout << endl;
                    cout << "Compra " << nome_compra << " foi removida do carrinho." << endl << endl;
                    cout << "Escolha uma opção:" << endl;
                    cout << "1 - Adicionar uma compra ao carrinho." << endl;
                    cout << "2 - Finalizar a venda." << endl;
                    cin >> op;
                    if(op == 1){
                        add_compra();
                    }else{
                        imprime_carrinho();
                    }
                    entrada.close();
                }
            }else{
                cout << endl;
                cout << "Este produto não existe no carrinho." << endl << endl;
                cout << "Escolha uma opção:" << endl;
                cout << "1 - Adicionar uma compra ao carrinho." << endl;
                cout << "2 - Finalizar venda." << endl;
                cin >> op;
                if(op == 1){
                    add_compra();
                }else{
                    imprime_carrinho();
                }
            }
        }
    }
}

bool Carrinho::venda(){

    string y_or_n;
    int temp;

    system("clear");
    cout << "MODO VENDA" << endl << endl;
    cout << "Para realizar a venda, informe o CPF do cliente." << endl;
    cout << "Digite o CPF:" << endl;
    cin >> cpf;
    if(!file_exists_carrinho("../ep1/doc/Clientes/", cpf)){
        do{
            cout << endl;
            cout << "Cliente não cadastrado! Escolha uma opcao:" << endl;
            cout << "1 - Tentar novamente." << endl;
            cout << "2 - Voltar ao menu para cadastrar o cliente." << endl;
            cin >> temp;
            if(temp == 1){
                cout << endl;
                cout << "Digite o CPF do cliente novamente:" << endl;
                cin >> cpf;
            }else{
                return true;
            }
        }while(!file_exists_carrinho("../ep1/doc/Clientes/", cpf));
    }else{
        cout << endl;
        cout << "Cliente ";
        entrada.open("../ep1/doc/Clientes/" + cpf + ".txt");
        if(entrada.is_open()){
            if(getline(entrada, txt)){
                cout << txt << " está cadastrado";
            }
            if(text_exists_carrinho(cpf, "1")){
                associado = 1;
                cout << " e é asssociado!" << endl;
            }else{
                cout << " mas não é associado." << endl << endl;
            }
            entrada.close();
            cout << "Digite ENTER para ir ao modo de adição de produtos ao carrinho." << endl;
            getchar(); getchar();
            add_compra();
        }
    }
    return false;
}

