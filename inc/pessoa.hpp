#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <sys/types.h>
#include <dirent.h>
#include <bits/stdc++.h>
#include<string>

using namespace std;

class Pessoa{
	
   private:
         
    string nome;
	string cpf;
   public:

	Pessoa();
	~Pessoa();

	string get_nome();
	void set_nome(string nome);
	string  get_cpf();
	void set_cpf(string cpf);
	virtual bool cadastro();
};
#endif
